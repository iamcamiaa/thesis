<?php 

Route::get('/', 'UserController@sharedStories');

Route::get('/about', function(){
	return view('about');
});

Route::view('/adminlogin', 'Auth.admin-login');
Route::post('/verifyadmin', 'AdminHomeController@adminLogin');

Auth::routes();
Route::get('users/confirmation/{token}', 'Auth\RegisterController@confirmation')->name('confirmation');
route::middleware(['auth'])->group(function(){

Route::get('/getDen','SponsorController@getDen');


route::get('/home', 'PatientController@displayPatient');
route::get('/patientsdetail', 'PatientController@newPatient');
route::post('/singlelist', 'PatientController@savePatient');
route::get('/list/{id}/view','PatientController@patient');
route::get('/update/{id}/view','PatientController@updatepage');
route::get('/history', 'UserController@history');
Route::get('/donateSponsor', 'SponsorController@displaySponsorHistory');
Route::get('/sponsorDonate/{patientid}', 'SponsorController@newSponsor');
Route::get('/donateAny/{sponsorid}','SponsorController@newSponsorAny');
Route::post('/helpxp', 'SponsorController@saveSponsor');
Route::post('/homepage', 'SponsorController@saveSponsorAny');
Route::get('/confirm', 'UserController@voucher');
Route::post('/redeem', 'UserController@redeem');
Route::get('/total', 'UserController@total');

Route::get('/buyvoucher/{userid}', 'UserController@buyvoucher');
Route::post('/voucher', 'UserController@savevoucher');
Route::get('/update/{patientid}', 'PatientController@updateStory');
Route::post('/saveUpdate', 'PatientController@saveupdateStory');
//ADMIN Routes
Route::get('/displaypatients', 'AdminHomeController@viewPatients');
Route::get('/displaysponsors', 'AdminHomeController@viewSponsors');
Route::get('/delete', 'AdminHomeController@deleteUsers');
Route::get('/patientsponsor/{userid}', 'AdminHomeController@patientSponsor');
Route::get('/sponsorsponsored/{userid}', 'AdminHomeController@sponsorSponsored');
Route::get('/patienthistory/{userid}', 'AdminHomeController@patienthistory');
Route::get('/sponsorhistory/{userid}', 'AdminHomeController@sponsorhistory');
Route::post('/displaystats', 'AdminHomeController@stats');
Route::get('/displayusers', 'AdminHomeController@viewUsers');
Route::post('/filterpatient', 'AdminHomeController@filterPatientDate'); //not working







});


