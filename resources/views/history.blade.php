@extends('layouts.historyui')
@section('content')
<?php
use App\Donation;

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
setInterval(function(){
    $(document).ready(function (){
      var to = 0;
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/total',
            data:{
                '_token': $('input[name=_token]').val()
            },
            success:function(data){
                for(var i=0; i<data.length; i++){
                    to+=data[i].TotalRedeem;
                }
                $('#total').text(to);
                console.log(to);
            },
            error:function(){
            }
        });
    });
},3000);
</script>


<br>

    <br>
<div class="container2" style="padding: 0;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="font-size: 14pt; color: black;">Current Story</div>
                <div class="card-body">
                <form action="{{url('/confirm')}}">
                    <table width="100%" border="1" style="font-size: 13pt;">
                    <tr>
                        <th><center>Date</center></th>
                        <th><center>Goal</center></th>
                        <th><center>Redeem</center></th>
                        <th><center>Total</center></th>
                       
                    </tr> 

                    @foreach ($patientDetails as $patients)
                    <tr>
                        <td>{{$patients->created_at->format('d M Y')}}</td>
                        <td><center>{{$patients->goal}}</center></td>
                        <td><center><input type="submit" class="btn btn-primary" name="Redeem" value="Redeem" <?php if (isset($patients['status'])){ ?> disabled <?php   } ?> >
                        <td><center><div id="total"></center></div></td>
                    </tr>
                    @endforeach
                    </table>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div class="container2">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="font-size: 14pt; color: black;">Redeemed</div>
                <div class="card-body">
                <form action="{{url('/confirm')}}">
                    <table width="100%" border="1" style="font-size: 13pt;">
                    <tr>
                        <th><center>Date</center></th>
                        <th><center>Goal</center></th>
                        <th><center>Amount Redeemed</center></th>
                       
                    </tr> 
                    @foreach ($redeemdetails as $redeem)
                    <tr>
                       <td>{{$redeem->created_at->format('d M Y')}}</td>
                       <td>{{$redeem->goal}}</td>
                       <td>{{$redeem->TotalRedeem}}</td>
                    </tr>
                   @endforeach
                    </table>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>



<br>


<div class="container2">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="font-size: 14pt; color: black;">History of Donations</div>
                <div class="card-body">
                    <table width="100%" border="1" style="font-size: 13pt;">
                    <tr>
                        <th><center>Date</center></th>
                        <th><center>Amount Needed</center></th>
                        <th><center>To whom</center></th>
                    </tr> @foreach ($sponsorCollect as $sponsors)
                    <tr>
                    
                        <td>{{$sponsors->created_at->format('d M Y')}}</td>
                        <td>{{$sponsors->voucherValue}}</td>
                        @foreach ($sponsors->donation as $sponsors)
                        @if($sponsors->patient == null)
                        <td></td>
                        @else
                        <td>{{$sponsors->patient->userName->name}}</td>
                        @endif
                        @endforeach
                        @endforeach
                    </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>          <br><br>


@endsection
