@extends('layouts.about')
@section('content')

<div id="fh5co-feature-product" class="fh5co-section-white">
			<div class="container">
				<div class="row">
<div style="padding: 15px;">
	 <h3 style="letter-spacing: 0.2em"><strong>BENEFICIARY</strong></h3>
	 <h4 style="font-style: italic; letter-spacing: 0.2em; font-size: 15pt">Step up for a life challenge.</h4><br><p style= "letter-spacing: 0.1em; font-size: 14pt">
	 	Start fundraise for your hospital needs by just posting your story including the amount of money needed. </p><br>
	 	<p style= "letter-spacing: 0.1em; font-size: 13pt">
	 	Step 1: Create an account. <br><br>
	 	Step 2: Apply Sponsorship. <br><br>
	 	Step 3: Create your story and fill up the necessary information. Then post it! <br><br>
	 	Step 4: Your story will now be posted to our story page and is open for donations. <br>
	 	</p></div>
  </div>
</div>
</div>


<div id="fh5co-feature-product" class="fh5co-section-gray">
			<div class="container">
				<div class="row">
<div style="padding: 15px;">
	 <h3 style="letter-spacing: 0.2em"><strong>BENEFACTOR</strong></h3>
	 <h4 style="font-style: italic; letter-spacing: 0.2em; font-size: 15pt">Help out a mate in need.</h4><br>
	 <p style=" letter-spacing: 0.1em; font-size: 14pt">You can raise funds to support one's medical treatment by simply donating to that specific patient/person or by buying vouchers to our system. Donating to HELPXP is another way to give a little love.</p><br>
	 	<p style= "letter-spacing: 0.1em; font-size: 13pt">
	 	Step 1: Create an account. <br><br>
	 	Step 2: After creating an account, buy voucher to our system. <br><br>
	 	Step 3: You can now donate to HELPXP or select a specific patient you wanted to help. 
	 	Note: Donate to HELPXP (alternative) --- HELPXP will choose the patient to donate money to.    
	 	<br><br>
	 	</p>

</div>

  </div>
</div>



@endsection


