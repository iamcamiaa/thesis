

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>

	#box{
		width: 35%;
		margin: auto; 
		height: 11cm;

	}
	#innerbox{
		margin: auto;
		background-color: #27435a;
		height: 70px;
		width: 100%;

	}
	#line{
		border: 2px solid #ffaf40;
		width: 250px;
		margin: auto; 
	}
	#header{
		text-align: center;
		color: #ffaf40; 
		font-size: 20px;
		font-weight: bold;
		padding: 5px;

	}
	#greetings{
		font-size: 15px;
		color: black;
	}
	#first{
		font-weight: lighter;
		font-size: 15px;
		color: black;
	}
	#note{
		color: black;
		font-size: 15px;
	}
	#link{
		padding: 5px;
		font-size: 15px;
		border: 3px solid #ffaf40;
		background-color: #ffaf40;
		border-radius: 2px;
		color: #27435a;
		text-decoration: none;
	}
	#note1{
		color: black;
		font-size: 15px;
		margin-top: 20px;
	}
	#line1{
		border: 1px solid black;
		width: 12cm;
		margin: auto; 
	}
	#head{
		text-align: center;
		color: black; 
		font-size: 15px;
		font-weight: bold;
	}
	#second{
		font-weight: lighter;
		font-size: 15px;
		color: black;
		
		/*margin-top: -110px;*/
	}
	
	#reserved{
		padding: 20px;
		color: black;
		/*margin-top: -150px;*/
		font-size: 10px;
	}



</style>
</head>
<body>


<div id="box">
	<div id="innerbox">
		<p id="header">WELCOME TO HELPXP</p>
		<div id="line"></div>
	</div><br>
		<div id="greetings">&nbsp;Dear {{$name}},</div>

		<div id="first">
			&emsp;&emsp;&emsp;You're receiving this message because you signed up for an account on HELPXP.</div>

			<center><div id="note">(If you didn't sign up, you can ignore this email.)</div></center><br>

		<center><a href="{{ route('confirmation', $token)}}" id="link">Confirm your Email</a></center>

		<center><div id="note1">or confirm your email by clicking:</div></center>
		<center><br>{{ route('confirmation', $token)}}</center><br><br>

		<div id="line1"></div>

		<p id="head">HELPXP</p>

		<center><div id="second">Please verify your email address to finally complete your registration and activate your account. Thank you for registering your HelpXP Account. </div></center>

		<center><div id="reserved">If you prefer not to receive emails like this from Acclaim, you may unsubscribe. Manage your email notification preferences. &copy;Copyright 2018. All right reserved.</div></center>
</div>




</body>
</html>


