 @extends('layouts.patient')

@section('content')

<!-- <div class="about" style="width: 25%; height: auto">
  <div class="desc" style="background-color: lightgreen; border: 1px solid #ccc">
	 requirements:
	 <br>original copy of medical abstract
	 <br>medical certification
	 <br>Valid ID's (voter's ID, passport, etc.)
	 <br>hospital bill
	 <br>official quotation of breakdown of expenses
  </div>
</div>  -->

<head>

	
	<link rel="stylesheet" href="/css/form-basic.css">


</head>

<br><br>

    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <form class="form-basic" action="{{url('/singlelist')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}

            <div class="form-title-row">
                <h1>Share your Story!</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>Goal Title:</span>
                    <input type="text" name="title">
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Post Story</span>
                    <textarea name="story" rows="4" cols="60"></textarea>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Goal</span>
                    <input type="text" name="goal">
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Add a profile goal</span>
                    <span style="padding-left: 0;"><input type="file" name="profile"></span>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Medical Abstract</span>
                    <span style="padding-left: 0;"><input type="file" name="medicalAbstract"></span>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Medical Certification</span>
                    <span style="padding-left: 0;"><input type="file" name="medicalCertificate"></span>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Valid ID</span>
                    <span style="padding-left: 0;"><input type="file" name="validID"></span>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Hospital Bill</span>
                    <span style="padding-left: 0;"><input type="file" name="hospitalBill"></span>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Breakdown of Expenses</span>
                    <span style="padding-left: 0;"><input type="file" name="breakdownExpenses"></span>
                </label>
            </div>


            <div class="form-row">
                <label>
                    <span>Beneficiary Name</span>
                    <input type="text" name="bname">
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Illness</span>
                    <input type="text" name="illness">
                </label>
            </div>

            <div >
            	<label>
               		<span style="font-color: white"><input type="submit" class="btn btn-primary" value="Submit">
                	<input type="reset" class="btn btn-danger" value="Reset"></span>
            	</label>
            </div>

        </form>

    </div>
    <br><br>

</body>

</html>




@endsection 