@extends('layouts.voucher')

 
@section('content')


<head>

    <link rel="stylesheet" href="/css/form-basic.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

</head>


<br><br>
    <div class="main-content" style="">


        <form class="form-basic" action="{{url('/voucher')}}" method="post">
        	{{csrf_field()}}
        
            <div class="form-title-row">
                <h1 style="font-size: 45px;font-family: Verdana">Buy Vouchers<img src="/images/arrows.png" height="10%" width="15%" /></h1>
            </div>

            <div class="form-row">
                <label>
                    <span><input type="checkbox" name="check100" value="100"></span>&nbsp;<img src="/images/v_100.JPG" height="20%" width="60%" /><br>
                    
            
            <input type='button' name='add' onclick='javascript: document.getElementById("qty100").value++;getTotal1()' value='+' style="width: 40px" />
            <input type='text' name='qty100' id='qty100' style="width: 60px" />
            <input type='button' name='subtract' onclick='javascript: document.getElementById("qty100").value--;getTotal1()' value='-' style="width: 40px"/>
            &nbsp;
            <div id="totalVoucher"></div>

                </label>
            </div>

            <div class="form-row">
                <label>
                    <span><input type="checkbox" name="check500" value="500"></span>&nbsp;<img src="/images/v_500.JPG" height="20%" width="60%" /><br>
            
            <input type='button' name='add' onclick='javascript: document.getElementById("qty500").value++;getTotal2()' value='+' style="width: 40px" />
            <input type='text' name='qty500' id='qty500' style="width: 60px" />
            <input type='button' name='subtract' onclick='javascript: document.getElementById("qty500").value--;getTotal2()' value='-' style="width: 40px"/>
            &nbsp;
            <div id="totalVoucher2"></div>

                </label>
            </div>

            <div class="form-row">
                <label>
                    <span><input type="checkbox" name="check1000" value="1000"></span>&nbsp;<img src="/images/v_1000.JPG" height="20%" width="60%" /><br>
            
            <input type='button' name='add' onclick='javascript: document.getElementById("qty1000").value++;getTotal3()' value='+' style="width: 40px" />
            <input type='text' name='qty1000' id='qty1000' style="width: 60px" />
            <input type='button' name='subtract' onclick='javascript: document.getElementById("qty1000").value--;getTotal3()' value='-' style="width: 40px"/>
            &nbsp;
            <div id="totalVoucher3"></div>

                </label>
            </div>

            <div class="form-row">
                <label>
                    <span><input type="checkbox" name="check5000" value="5000"></span>&nbsp;<img src="/images/v_5000.JPG" height="20%" width="60%" /><br>
            
            <input type='button' name='add' onclick='javascript: document.getElementById("qty5000").value++;getTotal4()' value='+' style="width: 40px" />
            <input type='text' name='qty5000' id='qty5000' style="width: 60px" />
            <input type='button' name='subtract' onclick='javascript: document.getElementById("qty5000").value--;getTotal4()' value='-' style="width: 40px"/>
            &nbsp;
            <div id="totalVoucher4"></div>

                </label>
            </div>
<br>
            <div class="form-row"></div>
            <p style="color:grey; font-size: 10px">*Deposit your payment to HelpXP's Bank account.</p>
           <h3 style="font-weight: bold;font-family: Arial;color:grey"><img src="/images/lock.png" height="3%" width="5%" />&nbsp;HelpXP's Bank Account</h3>
            <p style="font-weight: bolder;color: grey;font-size: 30px;border: 1px solid #ccc">10987654321</p>
<br>
            <div >
            	<label>
               		<span style="font-color: white"><input type="submit" class="btn btn-primary" value="Submit">
                	<input type="reset" class="btn btn-danger" value="Reset"></span>
            	</label>
            </div>
                <p style="color: grey">*By continuing you agree to HelpXP's terms and policy</p>
        </form>

    </div>


<br><br>

<script>
    var v1 = getElementById('qty100').value;
    var v2 = getElementById('qty500').value;
    var v3 = getElementById('qty1000').value;
    var v4 = getElementById('qty5000').value;

function getTotal1()
{
   
   var total = v1 * 100;
   //display the result
    document.getElementById('totalVoucher').innerHTML = "Total Vouchers P "+ parseInt(total);

}
function getTotal2()
{
   
   var total = v2 * 500;
   //display the result
    document.getElementById('totalVoucher2').innerHTML = "Total Vouchers P "+ parseInt(total);
}
function getTotal3()
{
   
   var total = v3 * 1000;
   //display the result
    document.getElementById('totalVoucher3').innerHTML = "Total Vouchers P "+ parseInt(total);
}
function getTotal4()
{
   
   var total = v4 * 5000;
   //display the result
    document.getElementById('totalVoucher4').innerHTML = "Total Vouchers P "+ parseInt(total);
}
</script>

@endsection



