
<body>

<div class="container">
<!--   <h1><span style="color: blue">HELP</span>XP</h1>
  <h2>It's all about you.</h2>  --> 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item">
        <img src= "{{ url("/images/helping-hand-agency.jpg") }}" alt="HELPXP its all about you" style="max-height: 50%;max-width: 100%; min-height: 20%">
      </div>

      <div class="item active">
        <img src= "{{ url("/images/kids.jpg") }}" style="width:100%;">
      </div>
    
      <div class="item">
        <img src= "{{ url("/images/helping-hand-agency.jpg") }}" style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

</body>

