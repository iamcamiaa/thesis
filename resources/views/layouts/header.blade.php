<?php 
use App\Patient;
?>

<meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link rel="shortcut icon" href="favicon.ico">

  <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'> -->
  
  <!-- Animate.css -->
  <link rel="stylesheet" href="css/animate.css">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="css/icomoon.css">
  <!-- Bootstrap  -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <!-- Superfish -->
  <link rel="stylesheet" href="css/superfish.css">

  <link rel="stylesheet" href="css/style.css">


  <!-- Modernizr JS -->
  <script src="js/modernizr-2.6.2.min.js"></script>
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="js/respond.min.js"></script>
  <![endif]-->

  <script src="/js/jquery.min.js"></script>
  <!-- jQuery Easing -->
  <script src="/js/jquery.easing.1.3.js"></script>
  <!-- Bootstrap -->
  <script src="/js/bootstrap.min.js"></script>
  <!-- Waypoints -->
  <script src="/js/jquery.waypoints.min.js"></script>
  <script src="/js/sticky.js"></script>

  <!-- Stellar -->
  <script src="/js/jquery.stellar.min.js"></script>
  <!-- Superfish -->
  <script src="/js/hoverIntent.js"></script>
  <script src="/js/superfish.js"></script>
  
  <!-- Main JS -->
  <script src="/js/main.js"></script>

  </head>
  <body>
  @guest
    <!-- <div id="fh5co-wrapper">
    <div id="fh5co-page">
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 text-left fh5co-link">
            <a href="#">FAQ</a>
            <a href="#">Forum</a>
            <a href="#">Contact</a>
          </div>
          <div class="col-md-6 col-sm-6 text-right fh5co-social">
            <a href="#" class="grow"><i class="icon-facebook2"></i></a>
            <a href="#" class="grow"><i class="icon-twitter2"></i></a>
            <a href="#" class="grow"><i class="icon-instagram2"></i></a>
          </div>
        </div>
      </div>
    </div> -->
    <header id="fh5co-header-section" class="sticky-banner">
      <div class="container">
        <div class="nav-header">
          <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
          <h1 id="fh5co-logo"><a href="{{ url('/') }}">HELPXP</a></h1>
          <!-- START #fh5co-menu-wrap -->
          <nav id="fh5co-menu-wrap" role="navigation">
            <ul class="sf-menu" id="fh5co-primary-menu">
              <li>
                <a href="{{ url('/') }}">Home</a>
              </li>
              <li>
                <a href="#" class="fh5co-sub-ddown">Get Involved</a>
                <ul class="fh5co-sub-menu">
                  <li><a href="{{ url('/login') }}">Donate</a></li>
                  <li><a href="{{ url('/login') }}">Fundraise</a></li>
                  <li><a href="{{ url('/login') }}">Apply Sponsorships</a></li>
                </ul>
              </li>
              <li><a href="{{ url('/about') }}">About</a></li>
              <li class="active"><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  @else
    <header id="fh5co-header-section" class="sticky-banner">
      <div class="container">
        <div class="nav-header">
          <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
          <h1 id="fh5co-logo"><a href="{{ url('/home') }}">HELPXP</a></h1>

<?php
$user = Auth::id();
$patient = Patient::where('userid', $user)->where('status', '=', null)->get()->count(); 
?>
   
   <nav id="fh5co-menu-wrap" role="navigation">
            <ul class="sf-menu" id="fh5co-primary-menu">
              <li>
                <a href="{{ url('/home') }}">Home</a>
              </li>
              <li>
                <a href="#" class="fh5co-sub-ddown">Get Involved</a>
                <ul class="fh5co-sub-menu">
                  <li><a href="{{ url('/buyvoucher') }}/{{Auth::user()->id}}">Buy Voucher</a></li>
                  <li><a href="{{ url('/donateAny') }}/{{Auth::user()->id}}">Fundraise</a></li>
          @if($patient <= 0)
                  <li><a href="{{ url('/patientsdetail') }}">Apply Sponsorships</a></li>
          @else
          @endif
                </ul>
              </li>

   
              <li><a href="{{ url('/history') }}">History</a></li>
              <li><a href="{{ url('/about') }}">About</a></li>
              <li><a class="fh5co-sub-ddown" href="">Hi <strong>{{ Auth::user()->username }}</strong>!</a>
              <ul class="fh5co-sub-menu">
              <li><a href="{{ route('logout')}}" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>
              </ul>
              </li>
            
      <form id="logout-form" action="{{ route('logout') }}" method="POST">
      @csrf
      </form>
  
    </ul>
  </nav>
</div>
</div>
</header>

 @endguest


 <main class="py-4">
            @yield('content')
        </main>
        


</body>
</html>
   