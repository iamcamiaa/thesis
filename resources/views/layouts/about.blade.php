<?php 
use App\Sponsor;
$sponsor = Sponsor::where('userid', Auth::id())->get();

?>


<?php 
use App\Patient;
?>
<head>
<meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link rel="shortcut icon" href="favicon.ico">

  <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'> -->
  
  <!-- Animate.css -->
  <link rel="stylesheet" href="/css/animate.css">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="/css/icomoon.css">
  <!-- Bootstrap  -->
  <link rel="stylesheet" href="/css/bootstrap.css">
  <!-- Superfish -->
  <link rel="stylesheet" href="/css/superfish.css">

  <link rel="stylesheet" href="/css/style.css">


  <!-- Modernizr JS -->
  <script src="/js/modernizr-2.6.2.min.js"></script>
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="js/respond.min.js"></script>
  <![endif]-->

  <script src="/js/jquery.min.js"></script>
  <!-- jQuery Easing -->
  <script src="/js/jquery.easing.1.3.js"></script>
  <!-- Bootstrap -->
  <script src="/js/bootstrap.min.js"></script>
  <!-- Waypoints -->
  <script src="/js/jquery.waypoints.min.js"></script>
  <script src="/js/sticky.js"></script>

  <!-- Stellar -->
  <script src="/js/jquery.stellar.min.js"></script>
  <!-- Superfish -->
  <script src="/js/hoverIntent.js"></script>
  <script src="/js/superfish.js"></script>
  
  <!-- Main JS -->
  <script src="/js/main.js"></script>

  </head>
  <body>
  @guest
    <!-- <div id="fh5co-wrapper">
    <div id="fh5co-page">
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 text-left fh5co-link">
            <a href="#">FAQ</a>
            <a href="#">Forum</a>
            <a href="#">Contact</a>
          </div>
          <div class="col-md-6 col-sm-6 text-right fh5co-social">
            <a href="#" class="grow"><i class="icon-facebook2"></i></a>
            <a href="#" class="grow"><i class="icon-twitter2"></i></a>
            <a href="#" class="grow"><i class="icon-instagram2"></i></a>
          </div>
        </div>
      </div>
    </div> -->
    <header id="fh5co-header-section" class="sticky-banner">
      <div class="container">
        <div class="nav-header">
          <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
          <h1 id="fh5co-logo"><a href="{{ url('/') }}">HELPXP</a></h1>
          <!-- START #fh5co-menu-wrap -->
          <nav id="fh5co-menu-wrap" role="navigation">
            <ul class="sf-menu" id="fh5co-primary-menu">
              <li>
                <a href="{{ url('/home') }}">Home</a>
              </li>
              <li>
                <a href="#" class="fh5co-sub-ddown">Get Involved</a>
                <ul class="fh5co-sub-menu">
                  <li><a href="{{ url('/login') }}">Buy Voucher</a></li>
                  <li><a href="{{ url('/login') }}">Fundraise</a></li>
                  <li><a href="{{ url('/login') }}">Apply Sponsorships</a></li>
                </ul>
              </li>
              <li class="active"><a href="{{ url('/about') }}">About</a></li>
              <li><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  @else
    
  <header id="fh5co-header-section" class="sticky-banner">
      <div class="container">
        <div class="nav-header">
          <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
          <h1 id="fh5co-logo"><a href="{{ url('/home') }}">HELPXP</a></h1>

<?php
$user = Auth::id();
$patient = Patient::where('userid', $user)->where('status', '=', null)->get()->count(); 
?>
   
   <nav id="fh5co-menu-wrap" role="navigation">
            <ul class="sf-menu" id="fh5co-primary-menu">
              <li>
                <a href="{{ url('/home') }}">Home</a>
              </li>
              <li>
                <a href="#" class="fh5co-sub-ddown">Get Involved</a>
                <ul class="fh5co-sub-menu">
                  <li><a href="{{ url('/buyvoucher') }}/{{Auth::user()->id }}">Buy Voucher</a></li>
                  <li><a href="{{ url('/donateAny') }}/{{Auth::user()->id }}">Fundraise</a></li>
          @if($patient <= 0)
                  <li><a href="{{ url('/patientsdetail') }}">Apply Sponsorships</a></li>
          @else
          @endif
                </ul>
              </li>

   
              <li><a href="{{ url('/history') }}">History</a></li>
              <li class="active"><a href="{{ url('/about') }}">About</a></li>
              <li><a class="fh5co-sub-ddown" href="">Hi <strong>{{ Auth::user()->name }}</strong>!</a>
              <ul class="fh5co-sub-menu">
              <li><a href="{{ route('logout')}}" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>
              </ul>
              </li>
            
      <form id="logout-form" action="{{ route('logout') }}" method="POST">
      @csrf
      </form>
  
    </ul>
  </nav>
</div>
</div>
</header>

<!-- start sa not yet fin -->
<div class="fh5co-hero">
      <div class="fh5co-overlay"></div>
      <div class="fh5co-cover text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/pov.jpg);">
        <div class="desc animate-box">
          <h2 style="letter-spacing: 0.1em"><strong>About Us</strong></h2><br>
          <h3 style="color: white; letter-spacing: 0.008">HELPXP is an online fundraising tool which aims to help indigent people in applying for help in paying hospital bills.</h3><br><br>
          @if (Sponsor::where('userid', Auth::id())->where('status', '=', null) == null)
          <span><a class="btn btn-primary btn-lg" href="{{ url('/buyvoucher') }}/{{Auth::user()->id }}">Donate Now</a></span>
          @else
          <span><a class="btn btn-primary btn-lg" href="{{ url('/donateAny') }}/{{Auth::user()->id }}">Donate Now</a></span>
          @endif
        </div>              
      </div>

    </div> <!-- end sa not yet fin -->

    <div id="fh5co-features">
      <div class="container">
        <div class="row">
          <div class="col-md-4">

            <div class="feature-left">
              
              <div class="feature-copy">
                <h3>Become a member</h3>
                <p>Be a member and help people in need. For sharing is loving.</p>
                <p><a href="#">Learn More</a></p>
              </div>
            </div>

          </div>

          <div class="col-md-4">
            <div class="feature-left">
              
              <div class="feature-copy">
                <h3>Happy Giving</h3>
                <p>Giving back is as good for you as it is for those you are helping.</p>
                <p><a href="#">Learn More</a></p>
              </div>
            </div>

          </div>
          <div class="col-md-4">
            <div class="feature-left">
              
              <div class="feature-copy">
                <h3>Donation</h3>
                <p>Making money is a happiness; making other people happy is a superhappiness.</p>
                <p><a href="#">Learn More</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="fh5co-feature-product" class="fh5co-section-gray" style="margin-bottom: 2px;">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center heading-section">
            <h3>How it works.</h3>
             
          </div>
<main class="py-4">
            @yield('content')
        </main>
        </div>
      </div>
    </div>

 @endguest

<br><br><br>



<div class="row">
          <div class="col-md-4 col-md-offset-4 text-center animate-box">
            <a href="#" class="btn btn-primary btn-lg">Raise Funds</a>
          </div>
</div>
<br><br><br><br><br>

<footer>
      <div id="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
              
              <p><a href="/">HELPXP</a>. All Rights Reserved. </a></p>
            </div>
          </div>
        </div>
      </div>
    </footer>


</body>
</html>
   