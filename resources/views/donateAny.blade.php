@extends('layouts.raise')
@section('content')


<head>


	<link rel="stylesheet" href="/css/form-basic.css">

</head>

<br><br>

    <div class="main-content">

        <!-- You only need this form and the form-register.css -->

        <form class="form-basic" action="{{url('/homepage')}}" method="post">

             {{csrf_field()}}

                    <div class="form-title-row">
                        <h1>Donate to HELPXP</h1>
                        <p style="font-style: normal; font-family: Arial Narrow; font-size: 11pt">Make an online donation to help our organization provide treatment, care and support to vulnerable children. Fill out the form below to send your donation. Thank you for your help!</p>
                    </div>

                    <div class="form-row">
                        <label>
                            <span>Amount to be donated:</span>
                            <input type="text" name="amount" id="amount">
                        </label>
                    </div>

                   
                    <div class="form-row" style="margin-left: 35px">
                        <button type="submit" name="submit">Donate</button>
                    </div>

                



        </form>

    </div>


<p id="den">
        <img src="" id="img">
</p>


@if(Session::has('info'))
<div class="alert alert-confirm">
    <script>
        confirm('You cannot donate your desired amount. You only have {{ Session::get('avblVoucher', '') }} available vouchers.');
    </script>  
</div>
@elseif(Session::has('alert'))
    <script>
        confirm('You cannot donate your desired amount. Please buy vouchers first.');
    </script>
@elseif(Session::has('success'))
    <script>
        alert('Successful Donation');
    </script>
@elseif(Session::has('message'))
    <script>
        alert('Cant donate with this amount');
    </script>
@elseif(Session::has('notenough'))
    <script>
        alert('You dont have enough vouchers');
    </script>
@endif


</div>
            </div>
        </div>
    </div>
</div>

<!-- <script>
function check(){
    var voucher = document.getElementById("voucher").value;
    var gl = document.getElementById("gl").value;
    var goal = document.getElementById("goal").value;

if(gl > voucher){
    alert("greater");
}
     // if(gl < 200)
     //     alert("minimum of 200");

    //  if(voucher < gl)
    //  alert("Your donation is greater");
    // else
    //  alert("success");
    
}
</script>
 -->

<!-- <script>   
        alert("donation has a remaining balance");
</script> -->
<p id="total"></p>

    <script type="text/javascript">
     $(document).ready(function (){
      var to = "";
      var total = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/getDen',
            data:{
                '_token': $('p[name=_token]').val()
            },
            success:function(data){
                for(var i=0; i<data.length; i++){
                    to += data[i].value+" = "+data[i].count+" ";
                    
                }   
                total += data[data.length - 1].total;                    
                
               
                $('#den').text(to);
                $('#total').text(total);
                console.log(data);
            },
            error:function(){
            }
        });
    });
</script>
         

<!-- <script>
function check(){
	var voucher = document.getElementById("voucher").value;
	var gl = document.getElementById("gl").value;
	var goal = document.getElementById("goal").value;

if(gl > voucher){
	alert("greater");
}
	 // if(gl < 200)
	 // 	alert("minimum of 200");

	//  if(voucher < gl)
	// 	alert("Your donation is greater");
	// else
	// 	alert("success");
	
}
</script>
 -->

<!-- <script>   
		alert("donation has a remaining balance");
</script> -->

@endsection

