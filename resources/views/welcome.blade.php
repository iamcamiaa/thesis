<?php 
use App\Sponsor;
$sponsor = Sponsor::where('userid', Auth::id())->get();

?>


@extends('layouts.welcome')
@section('content')


@if(Session::has('status'))
    <script>
        alert('Check your email');
    </script>
@elseif(Session::has('alert'))
    <script>
        alert('Something went wrong. Please come back later');
    </script>
@elseif(Session::has('success'))
    <script>
        alert('Your account is fully activated');
    </script>
@endif

<!-- start sa all time donations -->

  <!-- end sa all time donations -->



<!-- start div para sa stories -->

 @foreach ($data as $patients)
<div style="box-sizing: border-box; float: left; width: 25%; padding: 5px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$patients->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$patients['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$patients->userName->name}}</h2><br>
                    <div align="center"><input type="button" name="donate" value="Donate" class="btn btn-primary-info"></div>
                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>


@endforeach  

@endsection