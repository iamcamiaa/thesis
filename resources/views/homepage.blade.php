<?php 
use App\Sponsor;
$sponsor = Sponsor::where('userid', Auth::id())->get();

?>

@extends('layouts.home')
@section('content')

<br>

<!-- start sa all time donations -->
<div class="container3">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 35pt; letter-spacing: 0.2em"> 
   P <strong>{{number_format(DB::table('patients')->sum('TotalRedeem'))}}</strong> all time donations
  </div>
</div> 
</div>
  <!-- end sa all time donations -->

<br><br><br>
<div class="row">
  <div class="col-md-6 col-md-offset-3 text-center heading-section animate-box">
    <span style="font-size: 20pt">On-Going Stories</span>
    <p>Make an online donation to help our organization provide treatment, care and support to vulnerable people.</p>
</div>
</div>

<!-- start div para sa stories -->

 @foreach ($data as $patients)


<div style="box-sizing: border-box; float: left; width: 24%; padding: 8px; margin-left: 12px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$patients->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$patients['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$patients->userName->name}}</h2><br>
                    <div align="left"><input type="button" name="donate" value="+Read More" class="btn btn-primary-info"></div>
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$patients->TotalRedeem/$patients->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$patients->TotalRedeem/$patients->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px">P{{number_format($patients->TotalRedeem)}} raised of P{{number_format($patients->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>

@endforeach  

<br><br><br>
<div style=" margin-top: 60%">
<div class="row" >
  <div class="col-md-6 col-md-offset-3 text-center heading-section animate-box">
    <span style="font-size: 20pt">Success Stories</span>
</div>
</div>
</div>

<div class="row">
          <div class="col-md-4 col-md-offset-4 text-center animate-box">
            <a href="#" class="btn btn-primary btn-lg">Raise Funds</a>
          </div>
        </div>

        <br><br><br>

    <!-- fh5co-blog-section -->
    <footer>
      <div id="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
              
              <p><a href="#">HELPXP</a>. All Rights Reserved. </a></p>
            </div>
          </div>
        </div>
      </div>
    </footer>



@endsection



  










