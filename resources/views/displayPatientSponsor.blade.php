<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<style>
	ul {
    	list-style-type: none;
    	margin: 0;
    	padding: 0;
    	overflow: hidden;
    	background-color: #444444;
	}

	li {
    	float: left;
	}

	li a {
    	display: block;
    	color: white;
    	text-align: center;
    	padding: 14px 16px;
    	text-decoration: none;
	}
	.panel {
		background-color: #F8F6F6;
		
	}
	</style>
</head>
<body>

<ul>
  <li><a class="navbar-brand" href="{{ url('/displayusers') }}"><span style="font-family: Arial; color: #08A7D7; font-size: 15pt">HELP</span><span style="font-family: Arial; color: white; font-size: 15pt">XP</span></a></li>
  <!-- <li><a href="#news">News</a></li>
  <li><a href="#contact">Contact</a></li>
  <li><a href="#about">About</a></li> -->
</ul>
	

<div class="panel">
	<table class="table table-bordered">
	
	<tr>
	<th>Date</th>
	<th>Amount Donated</th>
	<th>Name</th>
	</tr>
	@foreach($patientCollect as $patients)
		 <td>{{$patients->created_at->format('d M Y')}}</td>
         <td>{{$patients->sponsor->amountDonated}}</td>
         <td>{{$patients->sponsor->user->name}}</td>
        
</tr>
	@endforeach

</table>
</div>

</body>
</html>