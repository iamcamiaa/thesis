@extends('layouts.updatepage')

@section('content')

<br><br>
<div style="background-color: #FFF; padding: 35px; max-width: 670px; width: 670px; margin-left: 28%">

@foreach ($ups as $u)

<strong style="letter-spacing: 0.1em; font-size: 13pt; color: black">{{$u->storytitle}}</strong><br>
<span style="letter-spacing: 0.1em; font-size: 11pt; color: #167ED8; font-style: italic;">Date Posted: {{$u->created_at->format('F d, Y')}}</span><br>
@foreach ($u->picture as $update)
<img style="margin-top: 7px;" src="{{  url('storage/picture/'.$update->filename)}}" width="600px" height="400px" /><br>
@endforeach
<div style="background-color: #F2F1F1; height: 200px; width: 600px;">
		<h4 style="margin-top: 5px; color: black; letter-spacing: 0.1em; padding: 15px">{{$u->story}}</h4>
	</div><br><br><br>

  @endforeach

</div> 
<!-- update end -->

	



@endsection
