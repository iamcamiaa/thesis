<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Sponsor;
use App\Patient;
use App\User;
use App\Donation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class SponsorController extends Controller
{
      
  public function newSponsor($patientid){
        $patient = Patient::findorfail($patientid);
      return view('sponsorDonate')->with(['patient'=>$patient]);
    }



  public function getDen()
    {
       $overall = 0;
$countvalue = Sponsor::select('voucherValue')->where('userid', Auth::id())->where('status', null)->distinct()->get();
      $value = array();
      $d = new Collection();
      foreach ($countvalue as $key) {
        $cnt = Sponsor::where('voucherValue',$key->voucherValue)->where('userid', Auth::id())->where('status', null)->get()->count();
        $total = $key->voucherValue * $cnt;
        $overall = $total+=$overall;
        $value['value'] = $key->voucherValue;
        $value['count'] = $cnt;
        
        $value['total'] = (string)$overall;
        $d->push($value);    
      }

      return $d;
  }


    public function saveSponsor(Request $request){ 
      $patient = Patient::findorfail($request->patientid);
      $lacking = $patient['goal'] - $patient['donations'];
      $amount = $request->amount;
      if($amount == 0){
        return Redirect::back()->with('message', true);
      }
      $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();
      $total = $sponsor->sum('voucherValue'); 
      $collect = new Collection();
      $V = new Collection();
      $amt = $amount;
if($amt <= $total && $amount != 0){
        foreach($sponsor as $c){ 
          if($amt - $c['voucherValue'] >= 0){
            $V->push($c);
            $amt -= $c['voucherValue'];
          }
        }
      

        $avblVoucher =  $V->sum('voucherValue');
      if($amt == 0){
        $total = $patient->TotalRedeem + $V->sum('voucherValue');
        $patient->TotalRedeem = $total;
        $patient->save();
          foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "donated";
          $s->save();
          $donation = new Donation();
          $donation->patientid = $patient->patientid;
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->save();
        }
          return Redirect::back()->with('success', true); 
        }else if ($avblVoucher != 0){      
          return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }else
          return Redirect::back()->with('alert', true);
}return Redirect::back()->with('notenough', true);
    }



    public function saveSponsorAny(Request $request){
      $amount = $request->amount;
      $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();  
      $total = $sponsor->sum('voucherValue'); 

      if($amount == 0){
        return Redirect::back()->with('message', true);
      }

      $collect = new Collection();
      $V = new Collection();
      $amt = $amount;
if($amt <= $total){
        foreach($sponsor as $c){ 
          if($amt - $c['voucherValue'] >= 0){
            $V->push($c);
            $amt -= $c['voucherValue'];
          }
        }
        

        $avblVoucher =  $V->sum('voucherValue');
        if($amt == 0){
          foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "donated";
          $s->save();
          $donation = new Donation();
          $donation->patientid = null;
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->save();
        }
          return Redirect::back()->with('success', true); 
        }else if ($avblVoucher != 0){    
          return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }else
          return Redirect::back()->with('alert', true);
}
return Redirect::back()->with('notenough', true);
    }

   
    public function newSponsorAny(){
     

         return view('donateAny');
    }


}
