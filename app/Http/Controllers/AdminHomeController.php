<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;


use App\Http\Requests;

use Auth;
use App\User;
use App\Patient;
use App\Sponsor;
use App\Donation;
use Carbon\Carbon;
use DB;


class AdminHomeController extends Controller
{
    public function adminLogin(Request $req) {
        //return $req;
        if (Auth::attempt(['username' => $req->username, 'password' => $req->password, 'role' => 'admin']))
        {
            return view('displayUsers');
        }
        else
            return view('auth.admin-login');
    }

    public function viewUsers()
    {
        if(Auth::user()->role == "admin"){
            

            return view('displayusers');
        }
        else
            return "ERROR!";

    }

    // public function deleteUsers(Request $request)
    // {
    //    $user = User::findOrFail($request->id);
    //    $user->delete();
    //    return redirect(url('/displayusers'));
    // }

    public function viewPatients()
    {
        if(Auth::user()->role == "admin"){
            $users = Patient::select('userid')->distinct('userid')->get();
            return view('displaypatients', compact('users'));
    }
        else
            return "ERROR!";
    }

    public function viewSponsors()
    {
        if(Auth::user()->role == "admin"){
            $users = Sponsor::select('userid')->distinct('userid')->get();
            return view('displaysponsors', compact('users'));
            }
        else
            return "ERROR!";
    }
    

    public function patientSponsor($userid) {

       //   $id = Donation::where('patientid', '=', $userid)->get();
       // return view('displayPatientSponsor')->with([ 'patientid' => $id]);

        if(Auth::user()->role == "admin"){
        $patient = Patient::where('userid', $userid)->get();
        $voucher = Donation::get();
        $patientCollect = new Collection();
        foreach($patient as $pts) {
            foreach ($voucher as $vcr) {
                if($pts['patientid'] == $vcr['patientid']) {
                    $patientCollect->push($vcr);
                 
                }
            }
            
        }
      
       return view('displayPatientSponsor')->with([ 'patientCollect' => $patientCollect]);
    }
    else
        return "ERROR!";

    }




    public function sponsorSponsored($userid) {
        if(Auth::user()->role == "admin"){
        $sponsor = Sponsor::where('userid', $userid)->get();
        $voucher = Donation::get();
        $sponsorCollect = new Collection();
        foreach($sponsor as $spr){
            foreach($voucher as $dnr){
                if($dnr['sponsorid'] == $spr['sponsorid']){
                $sponsorCollect->push($spr);
                }
            }
        }

         // $id = Donation::where('sponsorid', '=', $userid)->get();
      

        return view('displaySponsorSponsored')->with(['sponsorCollect'=>$sponsorCollect]);
    }

    else return "ERROR!";
    }

    public function patienthistory($userid) {
        if(Auth::user()->role == "admin"){
        $user = Auth::id();
        $patients = Patient::where('userid', "=" ,$userid)->get();
        return view('patienthistory')->with(['user'=>$patients]);
    }
        else 
            return "ERROR";
    }

    public function sponsorhistory($userid) {
        if(Auth::user()->role == "admin"){
        $user = Auth::id();
        $sponsors = Sponsor::where('userid', "=" ,$userid)->get();
        return view('sponsorhistory')->with(['user'=>$sponsors]);
         }
        else 
            return "ERROR";
    }

    public function filterPatientDate(Request $request){ 
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        $data = Patient::whereBetween(DB::raw('DATE(created_at)'), array($from, $to))->get();
         return response()->json($data);
            // if($data == true) {
            // $users = Patient::select('userid')->distinct('userid')->get();
            // return view('filterPatientDate', compact('users'));
            // }
            // else
            //     return "Noooo";

         }
        else 
            return "ERROR";
    }


}
