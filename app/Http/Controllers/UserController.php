<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sponsor;
use App\User;
use App\Donation;
use App\Patient;
use App\Stories;
use App\Picture;
use DB;

class UserController extends Controller
{
    public function history(){
//current story
        $user = Auth::id();
        
        $patient = Patient::where('userid', $user)->get();
        $patientDetails = [];
        foreach($patient as $pnt){
            $count = 0;
            
                if($pnt['status'] == null ){
                    $count++;   
                }
            
            if ($count != 0){
                array_push($patientDetails, $pnt);
            }   
        }

//sponsors                      
$sponsor = Sponsor::where('userid', $user)->get();
$donation = Donation::get();
$sponsorCollect = new Collection();
foreach($sponsor as $spr){
    foreach($donation as $dnr){
        if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
            $sponsorCollect->push($spr);
            }
        }
}

//redeem
       
        $patient = Patient::where('userid', $user)->get();
        $redeemdetails = [];
        foreach($patient as $pnt){
            $count = 0;
                if($pnt['status'] != null){
                    $count++;
                }
            
            if($count > 0)
                array_push($redeemdetails, $pnt);
        }

        return view('history')->with(['sponsorCollect'=>$sponsorCollect, 'patientDetails'=>$patientDetails, 'redeemdetails'=>$redeemdetails]);
    }










    public function voucher(){
        $user = Auth::id();
        $patient = Patient::where('userid', $user)->get();
        $details = [];
        foreach($patient as $pnt){
            $count = 0;
            
                if($pnt['status'] == null ){
                    $count++;   
                }
            
            if ($count != 0){
                array_push($details, $pnt);
            }   
        }

        $expirydate = date('Y-m-d', strtotime('+1 years'));
        return view('confirm')->with(['expirydate'=>$expirydate, 'details'=>$details]);
    }










    public function redeem(Request $request){
        $redeem = Patient::findorfail($request->id);
        $redeem->status = "redeemed";
        $redeem->expirydateV = $request->expirydate;
        $redeem->save();

        $pnt = Patient::get();

         $data = [];
         foreach($pnt as $p){          
                $count = 0;
                    if($p['status'] != null){  
                          $count++;

                    }
                    if($p['goal'] == $p['TotalRedeem']){
                        $count++;
                        
                    }
                
            if($count == 0)
            array_push($data, $p);
         }
        return view('homepage')->with(['data'=>$data]);
    }









    public function total(Request $request){
         $user = Auth::id();
        $patient = Patient::select('TotalRedeem')->where('userid', $user)->get();
        foreach($patient as $pnt){
            $data = [$pnt];
        }
        return response()->json($data);
    }



    public function buyvoucher(){
        return view('buyvoucher');
    }



    public function savevoucher(Request $request){
        //$text100 = $request->text100;


        for($i = 0; $i < $request->qty100; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = $request->check100;
        $user->status = null;
        $user->save();
        }
        for($i = 0; $i < $request->qty500; $i++){
            $user = new Sponsor();
            $user->userid = Auth::id();
            $user->voucherValue = $request->check500;
            $user->status = null;
            $user->save();
        }
        for($i = 0; $i < $request->qty1000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = $request->check1000;
        $user->status = null;
        $user->save();
        }
        for($i = 0; $i < $request->qty5000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = $request->check5000;
        $user->status = null;
        $user->save();
        }

        $pnt = Patient::get();
         $data = [];
         foreach($pnt as $p){          
                $count = 0;
                    if($p['status'] != null){  
                          $count++;

                    }
                    if($p['goal'] <= $p['TotalRedeem']){
                        $count++;
                        
                    }
              
           if($count == 0)
            array_push($data, $p);
         }
        return view('homepage')->with(['data'=>$data]);

    }

    public function sharedStories(Request $request){
         $pnt = Patient::get();
         $data = [];
         foreach($pnt as $p){          
                $count = 0;
                    if($p['status'] != null){  
                          $count++;

                    }
                    if($p['goal'] <= $p['TotalRedeem']){
                        $count++;
                        
                    }
              
           if($count == 0)
            array_push($data, $p);
         } 

         $success = [];
         foreach($pnt as $s){          
                $count = 0;
                    if($p['status'] == null){  
                          $count++;

                    }
                    if($p['goal'] != $p['TotalRedeem']){
                        $count++;
                        
                    }
           if($count == 0)
            array_push($success, $s);
         }  


       return view('welcome')->with(['data'=>$data, 'success'=>$success]);
    }

    
}
 