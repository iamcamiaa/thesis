<?php

use Illuminate\Database\Seeder;

use App\User;
class AdminUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$adminData = [
        	[
        		'name' => 'Camille Joyce Abucay',
        		'username' => 'camjoy',
       			'email' => 'camjoy@helpxp.com',
        		'address' => 'Tayud, Consolacion, Cebu',
        		'birthdate' => '1998-08-01',
       			'contact' => '09562760143',
        		'password' => bcrypt('helpxp'),
        		'role' => 'admin'
       		],

            [
                'name' => 'Angel Faith Tomongha',
                'username' => 'gela',
                'email' => 'gel@helpxp.com',
                'address' => 'Tugbongan, Consolacion, Cebu',
                'birthdate' => '1998-12-04',
                'contact' => '02545658569',
                'password' => bcrypt('helpxp'),
                'role' => 'admin'
            ],
            [
                'name' => 'Camille de Castro',
                'username' => 'camia',
                'email' => 'camia@helpxp.com',
                'address' => 'Guadalope, Cebu City',
                'birthdate' => '1999-06-26',
                'contact' => '09124512456',
                'password' => bcrypt('helpxp'),
                'role' => 'admin'
            ]

       	];

        		DB::table('users')->insert($adminData);

    }
}

